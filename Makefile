CC=gcc
CFLAGS=
LDFLAGS=-Zomf -Zhigh-mem
EXEEXT=.exe
FFDEFS=-Dffturbo
SMDEFS=-Dsmturbo
TBDEFS=-Dtbturbo
SOURCES=mozturbo.cpp
OBJECTS=ffturbo.o smturbo.o tbturbo.o
EXECUTABLES=ffturbo$(EXEEXT) smturbo$(EXEEXT) tbturbo$(EXEEXT)

all: $(SOURCES) $(OBJECTS) $(EXECUTABLES)

ffturbo$(EXEEXT): ffturbo.o
	$(CC) $(LDFLAGS) ffturbo.o -o $@

smturbo$(EXEEXT): mozturbo.cpp
	$(CC) $(LDFLAGS) smturbo.o -o $@

tbturbo$(EXEEXT): mozturbo.cpp
	$(CC) $(LDFLAGS) tbturbo.o -o $@

ffturbo.o: mozturbo.cpp
	$(CC) $(CFLAGS) $(FFDEFS) -c $? -o $@

smturbo.o: mozturbo.cpp
	$(CC) $(CFLAGS) $(SMDEFS) -c $? -o $@

tbturbo.o: mozturbo.cpp
	$(CC) $(CFLAGS) $(TBDEFS) -c $? -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLES)
	